<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jabatan extends Model
{
    protected $table = 'pegawai';
    protected $fillable = ['id', 'nama'];

    public function pegawai()
    {
        return $this->belongsTo(Pegawai::class);
    }
}
