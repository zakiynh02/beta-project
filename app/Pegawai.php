<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pegawai extends Model
{
    protected $table = 'pegawai';
    protected $fillable = ['id', 'nama', 'jenis_kelamin', 'telepon', 'alamat'];

    public function jabatan()
    {
        return $this->hasMany(Jabatan::class);
    }
}
