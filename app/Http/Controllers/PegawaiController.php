<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PegawaiController extends Controller
{
    public function index(Request $request)
    {
        if($request->has('cari')){
            $pegawai = \App\Pegawai::where('nama','LIKE','%'.$request->cari.'%')->get();
        }else{
            $pegawai = \App\Pegawai::all();
        }
        return view('pegawai.index',['pegawai' => $pegawai]);
    }

    public function create(Request $request)
    {
        \App\Pegawai::create($request->all());
        return redirect('/pegawai')->with('sukses','Data Berhasil DiInput!');
    }

    public function edit($id)
    {
        $pegawai = \App\Pegawai::find($id);
        return view('/pegawai/edit',['pegawai' => $pegawai]);
    }

    public function update(Request $request,$id)
    {
        $pegawai = \App\Pegawai::find($id);
        $pegawai->update($request->all());
        return redirect('/pegawai')->with('sukses','Data Berhasil DiInput!');
    }

    public function delete($id)
    {
        $pegawai = \App\Pegawai::find($id);
        $pegawai->delete();
        return redirect('/pegawai')->with('sukses','Data Berhasil DiHapus!');
    }
}
