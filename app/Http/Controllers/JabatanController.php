<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class JabatanController extends Controller
{
    public function index(Request $request)
    {
        if($request->has('cari')){
            $jabatan = \App\Jabatan::where('nama','LIKE','%'.$request->cari.'%')->get();
        }else{
            $jabatan = \App\Jabatan::all();
        }
        return view('jabatan.index',['jabatan' => $jabatan]);
    }

    public function create(Request $request)
    {
        \App\Jabatan::create($request->all());
        return redirect('/jabatan')->with('sukses','Data Berhasil DiInput!');
    }

    public function edit($id)
    {
        $jabatan = \App\Jabatan::find($id);
        return view('/jabatan/edit',['jabatan' => $jabatan]);
    }

    public function update(Request $request,$id)
    {
        $jabatan = \App\Jabatan::find($id);
        $jabatan->update($request->all());
        return redirect('/jabatan')->with('sukses','Data Berhasil DiInput!');
    }

    public function delete($id)
    {
        $jabatan = \App\Jabatan::find($id);
        $jabatan->delete();
        return redirect('/jabatan')->with('sukses','Data Berhasil DiHapus!');
    }
}
