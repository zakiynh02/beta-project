<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/pegawai', 'PegawaiController@index');
Route::post('/pegawai/create', 'PegawaiController@create');
Route::get('/pegawai/{id}/edit', 'PegawaiController@edit');
Route::post('/pegawai/{id}/update', 'PegawaiController@update');
Route::get('/pegawai/{id}/delete', 'PegawaiController@delete');

Route::get('/jabatan', 'JabatanController@index');
Route::post('/jabatan/create', 'JabatanController@create');
Route::get('/jabatan/{id}/edit', 'JabatanController@edit');
Route::post('/jabatan/{id}/update', 'JabatanController@update');
Route::get('/jabatan/{id}/delete', 'JabatanController@delete');
